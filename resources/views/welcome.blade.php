<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <form action="{{ url('/') }}" method="post" class="row">
            @csrf
            <section class="col-6 offset-3">
                <h1>Dodawanie liczb:</h1>
                <div class="form-group my-2">
                    @error('number_1')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <input type="text" name="number_1" class="form-control @error('number_1'){{ 'is-invalid' }}@enderror" value="{{ old('number_1') }}">
                </div>
                <div class="form-group my-2">
                    @error('number_2')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <input type="text" name="number_2" class="form-control @error('number_2'){{ 'is-invalid' }}@enderror" value="{{ old('number_2') }}">
                </div>
                <div class="form-group my-2">
                    <button type="submit" class="btn btn-secondary" name="submit" value="1">Dodaj</button>
                </div>
            </section>

            <section class="col-6 offset-3 mt-5">
                <h1>Teksty:</h1>
                <div class="form-group my-2">
                    @error('text')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <input type="text" name="text" class="form-control @error('text'){{ 'is-invalid' }}@enderror" value="{{ old('text') }}">
                </div>
                <div class="form-group my-2">
                    <button type="submit" class="btn btn-secondary" name="submit" value="2">Zapisz</button>
                </div>
            </section>
        </form>
        <div class="row mt-5">
            @if($numberSum !== null)
                <div class="col-6 offset-3 mb-5">
                    <div class="alert alert-success">
                        Suma podanych liczb wynosi: {{ $numberSum }}
                    </div>
                </div>
            @endif
            <div class="col-6 offset-3">
                <h1>Nasze teksty:</h1>
                <ul class="list-unstyled">
                    @forelse($texts as $text)
                        <li>{{ $loop->iteration }}. {{ $text }}</li>
                    @empty
                        <li class="text-danger text-center">Brak tekstów!</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </body>
</html>
