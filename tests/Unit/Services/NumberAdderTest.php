<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Services\NumberAdder;
use PHPUnit\Framework\TestCase;

class NumberAdderTest extends TestCase
{
    private NumberAdder $numberAdder;

    protected function setUp(): void
    {
        $this->numberAdder = new NumberAdder();
    }

    /**
     * @dataProvider provideCorrectData
     */
    public function testAddNumbers(float|int $number1, float|int $number2, float|int $result): void
    {
        self::assertSame(
            $result,
            $this->numberAdder
                ->addNumbers($number1, $number2),
        );
    }

    /**
     * @return mixed[]
     */
    public function provideCorrectData(): array
    {
        return [
            [1, 1, 2],
            [1, 1.0, 2.0],
            [1.0, 1.0, 2.0],
            [1.0, 1, 2.0],
            [0, 0, 0],
            [-1, -1, -2],
            [1, -1, 0],
            [-1, 1, 0],
        ];
    }
}
