<?php

declare(strict_types=1);

namespace Tests\Unit\Managers;

use App\Http\Requests\TestRequest;
use PHPUnit\Framework\TestCase;

use function sprintf;

class TestRequestTest extends TestCase
{
    private const SUBMIT_KEY = 'submit';
    private const NUMBER_1_KEY = 'number_1';
    private const NUMBER_2_KEY = 'number_2';
    private const TEXT_KEY = 'text';

    private const ADD_NUMBER_SUBMIT = 1;
    private const ADD_TEXT_SUBMIT = 2;

    public function testAuthorizeSuccess(): void
    {
        self::assertTrue(
            (new TestRequest())->authorize()
        );
    }

    public function testRulesSuccess(): void
    {
        self::assertSame(
            [
                self::NUMBER_1_KEY => [
                    $this->excludeIf(self::SUBMIT_KEY, (string) self::ADD_TEXT_SUBMIT),
                    'numeric',
                ],
                self::NUMBER_2_KEY => [
                    $this->excludeIf(self::SUBMIT_KEY, (string) self::ADD_TEXT_SUBMIT),
                    'numeric',
                ],
                self::TEXT_KEY => [
                    $this->excludeIf(self::SUBMIT_KEY, (string) self::ADD_NUMBER_SUBMIT),
                    'string',
                ]
            ],
            (new TestRequest())->rules()
        );
    }

    /**
     * @dataProvider provideCorrectIsAddTextData
     */
    public function testIsAddTextSuccess(int $submit, bool $result): void
    {
        $request = new TestRequest([], [
            'submit' => $submit,
        ]);

        self::assertSame($result, $request->isAddText());
    }

    /**
     * @return mixed[]
     */
    public function provideCorrectIsAddTextData(): array
    {
        return [
            [self::ADD_TEXT_SUBMIT, true],
            [self::ADD_NUMBER_SUBMIT, false],
        ];
    }

    /**
     * @dataProvider provideCorrectIsAddNumberData
     */
    public function testIsAddNumberSuccess(int $submit, bool $result): void
    {
        $request = new TestRequest([], [
            'submit' => $submit,
        ]);

        self::assertSame($result, $request->isAddNumber());
    }

    /**
     * @return mixed[]
     */
    public function provideCorrectIsAddNumberData(): array
    {
        return [
            [self::ADD_NUMBER_SUBMIT, true],
            [self::ADD_TEXT_SUBMIT, false],
        ];
    }

    public function testGetNumber1Success(): void
    {
        $request = new TestRequest([], [
            'number_1' => 1,
        ]);

        self::assertSame(1.0, $request->getNumber1());
    }

    public function testGetNumber2Success(): void
    {
        $request = new TestRequest([], [
            'number_2' => 1,
        ]);

        self::assertSame(1.0, $request->getNumber2());
    }

    public function testGetTextSuccess(): void
    {
        $request = new TestRequest([], [
            'text' => 'Test',
        ]);

        self::assertSame('Test', $request->getText());
    }

    private function excludeIf(string $field, string $value): string
    {
        return sprintf('exclude_if:%s,%s', $field, $value);
    }
}
