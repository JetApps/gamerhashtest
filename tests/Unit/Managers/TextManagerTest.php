<?php

declare(strict_types=1);

namespace Tests\Unit\Managers;

use App\Managers\TextManager;
use Illuminate\Support\Facades\Redis;
use PHPUnit\Framework\TestCase;

class TextManagerTest extends TestCase
{
    private const TEXTS_KEY = 'texts';

    private TextManager $textManager;

    protected function setUp(): void
    {
        $this->textManager = new TextManager();
    }

    /**
     * @dataProvider provideCorrectGetAllTextsData
     */
    public function testGetAllTextsSuccess(?string $value, int $count): void
    {
        Redis::shouldReceive('get')
            ->once()
            ->with(self::TEXTS_KEY)
            ->andReturn($value);

        $texts = $this->textManager
            ->getAllTexts();

        self::assertIsArray($texts);
        self::assertCount($count, $texts);
    }

    /**
     * @return mixed[]
     */
    public function provideCorrectGetAllTextsData(): array
    {
        return [
            [null, 0],
            ['["Test1","Test2"]', 2],
        ];
    }

    public function testAppendTextSuccess(): void
    {
        Redis::shouldReceive('get')
            ->once()
            ->with(self::TEXTS_KEY)
            ->andReturn('["Test1","Test2"]');

        Redis::shouldReceive('set')
            ->once()
            ->with(self::TEXTS_KEY, '["Test1","Test2","Test3"]')
            ->andReturn(true);

        self::assertTrue(
            $this->textManager
                ->appendText('Test3')
        );
    }
}
