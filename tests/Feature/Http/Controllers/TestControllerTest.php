<?php

declare(strict_types=1);

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;

class TestControllerTest extends TestCase
{
    private const INDEX_URI = '/';

    private const ADD_NUMBER_SUBMIT = 1;
    private const ADD_TEXT_SUBMIT = 2;

    public function testIndexActionGetMethodSuccess(): void
    {
        $response = $this->get(self::INDEX_URI);

        $response->assertStatus(200);
    }

    /**
     * @dataProvider provideIncorrectNumberValidationData
     */
    public function testIndexActionPostMethodNumberValidationFail(mixed $number1, mixed $number2): void
    {
        $response = $this->post('/', [
            'submit' => self::ADD_NUMBER_SUBMIT,
            'number_1' => $number1,
            'number_2' => $number2,
        ]);

        $response->assertStatus(302);
    }

    /**
     * @return mixed[]
     */
    public function provideIncorrectNumberValidationData(): array
    {
        return [
            ['', 1],
            ['abc', 1],
            [1, 'abc'],
            ['abc', 'abc'],
            [null, 1],
            [1, null],
            [null, null],
            [true, null],
            [null, true],
            [true, true],
            [false, false],
        ];
    }

    /**
     * @dataProvider provideCorrectNumberValidationData
     */
    public function testIndexActionPostMethodNumberSuccess(mixed $number1, mixed $number2): void
    {
        $response = $this->post('/', [
            'submit' => self::ADD_NUMBER_SUBMIT,
            'number_1' => $number1,
            'number_2' => $number2,
        ]);

        $response->assertStatus(200);
    }

    /**
     * @return mixed[]
     */
    public function provideCorrectNumberValidationData(): array
    {
        return [
            [1, 1],
            [-1, -1],
            [0, 0],
            [1.0, 0],
            [1.0, 1.0],
            [-1.0, -1.0],
        ];
    }

    /**
     * @dataProvider provideIncorrectTextValidationData
     */
    public function testIndexActionPostMethodTextValidationFail(mixed $text): void
    {
        $response = $this->post('/', [
            'submit' => self::ADD_TEXT_SUBMIT,
            'text' => $text,
        ]);

        $response->assertStatus(302);
    }

    /**
     * @return mixed[]
     */
    public function provideIncorrectTextValidationData(): array
    {
        return [
            [null],
            [''],
            [false],
            [true],
            [123],
        ];
    }

    public function testIndexActionPostMethodTextSuccess(): void
    {
        $response = $this->post('/', [
            'submit' => self::ADD_TEXT_SUBMIT,
            'text' => 'test',
        ]);

        $response->assertStatus(200);
    }
}
