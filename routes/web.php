<?php

declare(strict_types=1);

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Request;

Route::match([Request::METHOD_GET, Request::METHOD_POST],'/', [TestController::class, 'index']);
