<?php

declare(strict_types=1);

namespace App\Services;

class NumberAdder
{
    public function addNumbers(float|int $number1, float|int $number2): float|int
    {
        return $number1 + $number2;
    }
}
