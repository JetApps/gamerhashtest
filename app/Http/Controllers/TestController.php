<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\TestRequest;
use App\Managers\TextManager;
use App\Services\NumberAdder;
use Illuminate\Contracts\View\View;

class TestController extends Controller
{
    public function index(
        TestRequest $request,
        TextManager $textManager,
        NumberAdder $numberAdder
    ): View {
        $numberSum = null;

        if ($request->isMethod(TestRequest::METHOD_POST)) {
            if ($request->isAddNumber()) {
                $numberSum = $numberAdder->addNumbers(
                    $request->getNumber1(),
                    $request->getNumber2()
                );
            }

            if ($request->isAddText()) {
                $textManager->appendText(
                    $request->getText()
                );
            }
        }

        return view('welcome', [
            'texts' => $textManager->getAllTexts(),
            'numberSum' => $numberSum,
        ]);
    }
}
