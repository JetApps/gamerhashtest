<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use function sprintf;

class TestRequest extends FormRequest
{
    private const SUBMIT_KEY = 'submit';
    private const NUMBER_1_KEY = 'number_1';
    private const NUMBER_2_KEY = 'number_2';
    private const TEXT_KEY = 'text';

    private const ADD_NUMBER_SUBMIT = 1;
    private const ADD_TEXT_SUBMIT = 2;

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return mixed[][]
     */
    public function rules(): array
    {
        return [
            self::NUMBER_1_KEY => [
                $this->excludeIf(self::SUBMIT_KEY, (string) self::ADD_TEXT_SUBMIT),
                'numeric',
            ],
            self::NUMBER_2_KEY => [
                $this->excludeIf(self::SUBMIT_KEY, (string) self::ADD_TEXT_SUBMIT),
                'numeric',
            ],
            self::TEXT_KEY => [
                $this->excludeIf(self::SUBMIT_KEY, (string) self::ADD_NUMBER_SUBMIT),
                'string',
            ]
        ];
    }

    public function isAddText(): bool
    {
        return $this->getSubmitValue() === self::ADD_TEXT_SUBMIT;
    }

    public function isAddNumber(): bool
    {
        return $this->getSubmitValue() === self::ADD_NUMBER_SUBMIT;
    }

    public function getNumber1(): float
    {
        return (float) $this->get(self::NUMBER_1_KEY);
    }

    public function getNumber2(): float
    {
        return (float) $this->get(self::NUMBER_2_KEY);
    }

    public function getText(): string
    {
        return $this->get(self::TEXT_KEY);
    }

    private function getSubmitValue(): int
    {
        return $this->request
            ->getInt(self::SUBMIT_KEY);
    }

    private function excludeIf(string $field, string $value): string
    {
        return sprintf('exclude_if:%s,%s', $field, $value);
    }
}
