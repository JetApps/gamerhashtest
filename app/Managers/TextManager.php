<?php

declare(strict_types=1);

namespace App\Managers;

use Illuminate\Support\Facades\Redis;

class TextManager
{
    private const TEXTS_KEY = 'texts';

    /**
     * @return string[]
     */
    public function getAllTexts(): array
    {
        $rawTexts = $this->getRawTexts();

        if ($rawTexts === null) {
            return [];
        }

        return json_decode($rawTexts);
    }

    public function appendText(string $text): bool
    {
        $texts = $this->getAllTexts();
        $texts[] = $text;

        return $this->setRawTexts(json_encode($texts));
    }

    private function getRawTexts(): ?string
    {
        return Redis::get(self::TEXTS_KEY);
    }

    private function setRawTexts(string $texts): bool
    {
        return Redis::set(self::TEXTS_KEY, $texts);
    }
}
